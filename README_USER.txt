1)Contributors (in alphabetical order):

    Felipe Bordeu (FelipeBordeu@gmail.com)
    Domenico Borzacchiello (Domenico.Borzacchiello@ec-nantes.fr)
    Adrien Leygue (Adrien.Leygue@ec-nantes.fr)

2) Configuration

in matlab :
 
    addpath("/dir/to/PxdmfIOFormMatlab/Src")
